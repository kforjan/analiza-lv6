namespace Analiza_LV6_zad1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.buttonPlus = new System.Windows.Forms.Button();
            this.buttonMinus = new System.Windows.Forms.Button();
            this.buttonPuta = new System.Windows.Forms.Button();
            this.buttonPodjeljeno = new System.Windows.Forms.Button();
            this.buttonSin = new System.Windows.Forms.Button();
            this.buttonCos = new System.Windows.Forms.Button();
            this.buttonJednako = new System.Windows.Forms.Button();
            this.buttonLog = new System.Windows.Forms.Button();
            this.buttonSqrt = new System.Windows.Forms.Button();
            this.buttonAbs = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.labelUnos = new System.Windows.Forms.Label();
            this.labelRjesenje = new System.Windows.Forms.Label();
            this.buttonTocka = new System.Windows.Forms.Button();
            this.buttonBrisi = new System.Windows.Forms.Button();
            this.buttonAns = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(44, 110);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 73);
            this.button1.TabIndex = 2;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(125, 110);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 73);
            this.button2.TabIndex = 3;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(206, 110);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 73);
            this.button3.TabIndex = 4;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Button3_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(206, 189);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 72);
            this.button6.TabIndex = 7;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.Button6_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(125, 189);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 72);
            this.button5.TabIndex = 6;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.Button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(44, 189);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(75, 72);
            this.button4.TabIndex = 5;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.Button4_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(44, 267);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 72);
            this.button7.TabIndex = 10;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.Button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(125, 267);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 72);
            this.button8.TabIndex = 9;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.Button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(206, 267);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 72);
            this.button9.TabIndex = 8;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.Button9_Click);
            // 
            // buttonPlus
            // 
            this.buttonPlus.Location = new System.Drawing.Point(44, 378);
            this.buttonPlus.Name = "buttonPlus";
            this.buttonPlus.Size = new System.Drawing.Size(75, 23);
            this.buttonPlus.TabIndex = 11;
            this.buttonPlus.Text = "+";
            this.buttonPlus.UseVisualStyleBackColor = true;
            this.buttonPlus.Click += new System.EventHandler(this.ButtonPlus_Click);
            // 
            // buttonMinus
            // 
            this.buttonMinus.Location = new System.Drawing.Point(125, 378);
            this.buttonMinus.Name = "buttonMinus";
            this.buttonMinus.Size = new System.Drawing.Size(75, 23);
            this.buttonMinus.TabIndex = 12;
            this.buttonMinus.Text = "-";
            this.buttonMinus.UseVisualStyleBackColor = true;
            this.buttonMinus.Click += new System.EventHandler(this.ButtonMinus_Click);
            // 
            // buttonPuta
            // 
            this.buttonPuta.Location = new System.Drawing.Point(206, 378);
            this.buttonPuta.Name = "buttonPuta";
            this.buttonPuta.Size = new System.Drawing.Size(75, 23);
            this.buttonPuta.TabIndex = 13;
            this.buttonPuta.Text = "*";
            this.buttonPuta.UseVisualStyleBackColor = true;
            this.buttonPuta.Click += new System.EventHandler(this.ButtonPuta_Click);
            // 
            // buttonPodjeljeno
            // 
            this.buttonPodjeljeno.Location = new System.Drawing.Point(44, 407);
            this.buttonPodjeljeno.Name = "buttonPodjeljeno";
            this.buttonPodjeljeno.Size = new System.Drawing.Size(75, 23);
            this.buttonPodjeljeno.TabIndex = 14;
            this.buttonPodjeljeno.Text = "/";
            this.buttonPodjeljeno.UseVisualStyleBackColor = true;
            this.buttonPodjeljeno.Click += new System.EventHandler(this.ButtonPodjeljeno_Click);
            // 
            // buttonSin
            // 
            this.buttonSin.Location = new System.Drawing.Point(125, 407);
            this.buttonSin.Name = "buttonSin";
            this.buttonSin.Size = new System.Drawing.Size(75, 23);
            this.buttonSin.TabIndex = 15;
            this.buttonSin.Text = "sin";
            this.buttonSin.UseVisualStyleBackColor = true;
            this.buttonSin.Click += new System.EventHandler(this.ButtonSin_Click);
            // 
            // buttonCos
            // 
            this.buttonCos.Location = new System.Drawing.Point(206, 407);
            this.buttonCos.Name = "buttonCos";
            this.buttonCos.Size = new System.Drawing.Size(75, 23);
            this.buttonCos.TabIndex = 16;
            this.buttonCos.Text = "cos";
            this.buttonCos.UseVisualStyleBackColor = true;
            this.buttonCos.Click += new System.EventHandler(this.ButtonCos_Click);
            // 
            // buttonJednako
            // 
            this.buttonJednako.Location = new System.Drawing.Point(287, 45);
            this.buttonJednako.Name = "buttonJednako";
            this.buttonJednako.Size = new System.Drawing.Size(75, 23);
            this.buttonJednako.TabIndex = 17;
            this.buttonJednako.Text = "=";
            this.buttonJednako.UseVisualStyleBackColor = true;
            this.buttonJednako.Click += new System.EventHandler(this.ButtonJednako_Click);
            // 
            // buttonLog
            // 
            this.buttonLog.Location = new System.Drawing.Point(44, 436);
            this.buttonLog.Name = "buttonLog";
            this.buttonLog.Size = new System.Drawing.Size(75, 23);
            this.buttonLog.TabIndex = 18;
            this.buttonLog.Text = "ln";
            this.buttonLog.UseVisualStyleBackColor = true;
            this.buttonLog.Click += new System.EventHandler(this.ButtonLog_Click);
            // 
            // buttonSqrt
            // 
            this.buttonSqrt.Location = new System.Drawing.Point(125, 436);
            this.buttonSqrt.Name = "buttonSqrt";
            this.buttonSqrt.Size = new System.Drawing.Size(75, 23);
            this.buttonSqrt.TabIndex = 19;
            this.buttonSqrt.Text = "sqrt";
            this.buttonSqrt.UseVisualStyleBackColor = true;
            this.buttonSqrt.Click += new System.EventHandler(this.ButtonSqrt_Click);
            // 
            // buttonAbs
            // 
            this.buttonAbs.Location = new System.Drawing.Point(206, 436);
            this.buttonAbs.Name = "buttonAbs";
            this.buttonAbs.Size = new System.Drawing.Size(75, 23);
            this.buttonAbs.TabIndex = 20;
            this.buttonAbs.Text = "abs";
            this.buttonAbs.UseVisualStyleBackColor = true;
            this.buttonAbs.Click += new System.EventHandler(this.ButtonAbs_Click);
            // 
            // button0
            // 
            this.button0.Location = new System.Drawing.Point(287, 189);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(75, 73);
            this.button0.TabIndex = 21;
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.Click += new System.EventHandler(this.Button0_Click);
            // 
            // labelUnos
            // 
            this.labelUnos.AutoSize = true;
            this.labelUnos.Location = new System.Drawing.Point(44, 30);
            this.labelUnos.Name = "labelUnos";
            this.labelUnos.Size = new System.Drawing.Size(0, 17);
            this.labelUnos.TabIndex = 22;
            // 
            // labelRjesenje
            // 
            this.labelRjesenje.AutoSize = true;
            this.labelRjesenje.Location = new System.Drawing.Point(44, 58);
            this.labelRjesenje.Name = "labelRjesenje";
            this.labelRjesenje.Size = new System.Drawing.Size(0, 17);
            this.labelRjesenje.TabIndex = 23;
            // 
            // buttonTocka
            // 
            this.buttonTocka.Location = new System.Drawing.Point(287, 268);
            this.buttonTocka.Name = "buttonTocka";
            this.buttonTocka.Size = new System.Drawing.Size(76, 71);
            this.buttonTocka.TabIndex = 24;
            this.buttonTocka.Text = ".";
            this.buttonTocka.UseVisualStyleBackColor = true;
            this.buttonTocka.Click += new System.EventHandler(this.ButtonTocka_Click);
            // 
            // buttonBrisi
            // 
            this.buttonBrisi.Location = new System.Drawing.Point(287, 74);
            this.buttonBrisi.Name = "buttonBrisi";
            this.buttonBrisi.Size = new System.Drawing.Size(75, 23);
            this.buttonBrisi.TabIndex = 25;
            this.buttonBrisi.Text = "C";
            this.buttonBrisi.UseVisualStyleBackColor = true;
            this.buttonBrisi.Click += new System.EventHandler(this.ButtonBrisi_Click);
            // 
            // buttonAns
            // 
            this.buttonAns.Location = new System.Drawing.Point(287, 110);
            this.buttonAns.Name = "buttonAns";
            this.buttonAns.Size = new System.Drawing.Size(75, 73);
            this.buttonAns.TabIndex = 26;
            this.buttonAns.Text = "Ans";
            this.buttonAns.UseVisualStyleBackColor = true;
            this.buttonAns.Click += new System.EventHandler(this.ButtonAns_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(398, 497);
            this.Controls.Add(this.buttonAns);
            this.Controls.Add(this.buttonBrisi);
            this.Controls.Add(this.buttonTocka);
            this.Controls.Add(this.labelRjesenje);
            this.Controls.Add(this.labelUnos);
            this.Controls.Add(this.button0);
            this.Controls.Add(this.buttonAbs);
            this.Controls.Add(this.buttonSqrt);
            this.Controls.Add(this.buttonLog);
            this.Controls.Add(this.buttonJednako);
            this.Controls.Add(this.buttonCos);
            this.Controls.Add(this.buttonSin);
            this.Controls.Add(this.buttonPodjeljeno);
            this.Controls.Add(this.buttonPuta);
            this.Controls.Add(this.buttonMinus);
            this.Controls.Add(this.buttonPlus);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button buttonPlus;
        private System.Windows.Forms.Button buttonMinus;
        private System.Windows.Forms.Button buttonPuta;
        private System.Windows.Forms.Button buttonPodjeljeno;
        private System.Windows.Forms.Button buttonSin;
        private System.Windows.Forms.Button buttonCos;
        private System.Windows.Forms.Button buttonJednako;
        private System.Windows.Forms.Button buttonLog;
        private System.Windows.Forms.Button buttonSqrt;
        private System.Windows.Forms.Button buttonAbs;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Label labelUnos;
        private System.Windows.Forms.Label labelRjesenje;
        private System.Windows.Forms.Button buttonTocka;
        private System.Windows.Forms.Button buttonBrisi;
        private System.Windows.Forms.Button buttonAns;
    }
}

