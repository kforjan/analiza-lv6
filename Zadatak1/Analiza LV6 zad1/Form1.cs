using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Analiza_LV6_zad1
{
//Napravite aplikaciju  znanstveni kalkulator  koja ce  imati funkcionalnost
//znanstvenog kalkulatora, odnosno  implementirati osnovne(+,-,*,/)  i barem  5 
//naprednih(sin, cos, log, sqrt...) operacija.
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string unosString = "";
        string unosString2 = "";
        bool tocka = false;
        bool operacija = false;
        int operand = 0;
        double unosDecimal1 = 0;
        double unosDecimal2 = 0;
        double Ans = 0;
        bool ansPrazan = true;
        bool gotovo = false;
        bool praviAnsPostoji = false;

        private void Button1_Click(object sender, EventArgs e)
        {
            if(gotovo == true)
            {
                Brisi();
                gotovo = false;
            }
            unosString = unosString + "1";
            labelUnos.Text = unosString;
            if (operacija == false)
            {
                unosDecimal1 = double.Parse(unosString);
            }
            else
            {
                unosString2 = unosString2 + "1";
                unosDecimal2 = double.Parse(unosString2);
            }
                
            
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            if (gotovo == true)
            {
                Brisi();
                gotovo = false;
            }
            unosString = unosString + "2";
            labelUnos.Text = unosString;
            if (operacija == false)
            {
                unosDecimal1 = double.Parse(unosString);
            }
            else
            {
                unosString2 = unosString2 + "2";
                unosDecimal2 = double.Parse(unosString2);
            }
        }

        private void Button3_Click(object sender, EventArgs e)
        {
            if (gotovo == true)
            {
                Brisi();
                gotovo = false;
            }
            unosString = unosString + "3";
            labelUnos.Text = unosString;
            if (operacija == false)
            {
                unosDecimal1 = double.Parse(unosString);
            }
            else
            {
                unosString2 = unosString2 + "3";
                unosDecimal2 = double.Parse(unosString2);
            }
        }

        private void Button4_Click(object sender, EventArgs e)
        {
            if (gotovo == true)
            {
                Brisi();
                gotovo = false;
            }
            unosString = unosString + "4";
            labelUnos.Text = unosString;
            if (operacija == false)
            {
                unosDecimal1 = double.Parse(unosString);
            }
            else
            {
                unosString2 = unosString2 + "4";
                unosDecimal2 = double.Parse(unosString2);
            }
        }

        private void Button5_Click(object sender, EventArgs e)
        {
            if (gotovo == true)
            {
                Brisi();
                gotovo = false;
            }
            unosString = unosString + "5";
            labelUnos.Text = unosString;
            if (operacija == false)
            {
                unosDecimal1 = double.Parse(unosString);
            }
            else
            {
                unosString2 = unosString2 + "5";
                unosDecimal2 = double.Parse(unosString2);
            }
        }

        private void Button6_Click(object sender, EventArgs e)
        {
            if (gotovo == true)
            {
                Brisi();
                gotovo = false;
            }
            unosString = unosString + "6";
            labelUnos.Text = unosString;
            if (operacija == false)
            {
                unosDecimal1 = double.Parse(unosString);
            }
            else
            {
                unosString2 = unosString2 + "6";
                unosDecimal2 = double.Parse(unosString2);
            }
        }

        private void Button7_Click(object sender, EventArgs e)
        {
            if (gotovo == true)
            {
                Brisi();
                gotovo = false;
            }
            unosString = unosString + "7";
            labelUnos.Text = unosString;
            if (operacija == false)
            {
                unosDecimal1 = double.Parse(unosString);
            }
            else
            {
                unosString2 = unosString2 + "7";
                unosDecimal2 = double.Parse(unosString2);
            }
        }

        private void Button8_Click(object sender, EventArgs e)
        {
            if (gotovo == true)
            {
                Brisi();
                gotovo = false;
            }
            unosString = unosString + "8";
            labelUnos.Text = unosString;
            if (operacija == false)
            {
                unosDecimal1 = double.Parse(unosString);
            }
            else
            {
                unosString2 = unosString2 + "8";
                unosDecimal2 = double.Parse(unosString2);
            }
        }

        private void Button9_Click(object sender, EventArgs e)
        {
            if (gotovo == true)
            {
                Brisi();
                gotovo = false;
            }
            unosString = unosString + "9";
            labelUnos.Text = unosString;
            if (operacija == false)
            {
                unosDecimal1 = double.Parse(unosString);
            }
            else
            {
                unosString2 = unosString2 + "9";
                unosDecimal2 = double.Parse(unosString2);
            }
        }

        private void Button0_Click(object sender, EventArgs e)
        {
            if (gotovo == true)
            {
                Brisi();
                gotovo = false;
            }
            unosString = unosString + "0";
            labelUnos.Text = unosString;
            if (operacija == false)
            {
                unosDecimal1 = double.Parse(unosString);
            }
            else
            {
                unosString2 = unosString2 + "0";
                unosDecimal2 = double.Parse(unosString2);
            }
        }

        private void ButtonTocka_Click(object sender, EventArgs e)
        {
            if (gotovo == true)
            {
                Brisi();
                gotovo = false;
            }
            if (tocka == false && operacija == false)
            {
                unosString = unosString + ".";
            }
            if (tocka == false && operacija == true)
            {
                unosString = unosString + ".";
                unosString2 = unosString2 + ".";
            }
            tocka = true;
            labelUnos.Text = unosString;
        }

        private void ButtonPlus_Click(object sender, EventArgs e)
        {
            gotovo = false;
            if (operacija == false && (unosString != "" && unosString != "-")) {
                operand = 1;
                if (ansPrazan == true)
                {

                    unosString = unosString + " + ";
                    labelUnos.Text = unosString;
                    operacija = true;
                    tocka = false;

                }
                else
                {
                    labelRjesenje.Text = "";
                    unosDecimal2 = 0;
                    unosString2 = "";
                    unosDecimal1 = Ans;
                    unosString = (unosDecimal1).ToString() + " + ";
                    labelUnos.Text = unosString;
                    operacija = true;
                    tocka = false;
                }
            }
        }
        private void ButtonMinus_Click(object sender, EventArgs e)
        {
            if (operacija == false && (unosString != "-"))
            {
                operand = 2;
                if (unosString == "")
                {
                    unosString += "-";
                    labelUnos.Text = unosString;
                }
                else
                {

                    gotovo = false;
                    operand = 2;
                    if (ansPrazan == true)
                    {
                        unosString = unosString + " - ";
                        labelUnos.Text = unosString;
                        operacija = true;
                        tocka = false;
                    }
                    else
                    {
                        labelRjesenje.Text = "";
                        unosDecimal2 = 0;
                        unosString2 = "";
                        unosDecimal1 = Ans;
                        unosString = (unosDecimal1).ToString() + " - ";
                        labelUnos.Text = unosString;
                        operacija = true;
                        tocka = false;
                    }
                }
            }
        }

        private void ButtonPuta_Click(object sender, EventArgs e)
        {
            gotovo = false;
            if (operacija == false && (unosString != "" && unosString != "-"))
            {
                operand = 3;
                if (ansPrazan == true)
                {


                    unosString = unosString + " * ";
                    labelUnos.Text = unosString;
                    operacija = true;
                    tocka = false;

                }
                else
                {
                    labelRjesenje.Text = "";
                    unosDecimal2 = 0;
                    unosString2 = "";
                    unosDecimal1 = Ans;
                    unosString = (unosDecimal1).ToString() + " * ";
                    labelUnos.Text = unosString;
                    operacija = true;
                    tocka = false;
                }
            }
        }

        private void ButtonPodjeljeno_Click(object sender, EventArgs e)
        {
            if (operacija == false && (unosString != "" && unosString != "-"))
            {
                operand = 4;
                gotovo = false;
                if (ansPrazan == true)
                {
                    unosString = unosString + " / ";
                    labelUnos.Text = unosString;
                    operacija = true;
                    tocka = false;
                }
                else
                {
                    labelRjesenje.Text = "";
                    unosDecimal2 = 0;
                    unosString2 = "";
                    unosDecimal1 = Ans;
                    unosString = (unosDecimal1).ToString() + " / ";
                    labelUnos.Text = unosString;
                    operacija = true;
                    tocka = false;
                }
            }
        }

        private void ButtonSin_Click(object sender, EventArgs e)
        {
            if (operacija == false && unosString!="")
            {
                labelUnos.Text = "Sin(" + unosDecimal1 + ")";
                labelRjesenje.Text = (Math.Sin(unosDecimal1)).ToString();
                Ans = Math.Sin(unosDecimal1);
                gotovo = true;
                unosDecimal1 = Ans;
                ansPrazan = false;
                praviAnsPostoji = true;
                operacija = false;
            }
        }

        private void ButtonCos_Click(object sender, EventArgs e)
        {
            if(operacija == false && unosString != "")
            {
                labelUnos.Text = "Cos(" + unosDecimal1 + ")";
                labelRjesenje.Text = (Math.Cos(unosDecimal1)).ToString();
                Ans = Math.Cos(unosDecimal1);
                gotovo = true;
                unosDecimal1 = Ans;
                ansPrazan = false;
                praviAnsPostoji = true;
                operacija = false;
            }
        }

        private void ButtonLog_Click(object sender, EventArgs e)
        {
            if (operacija == false && unosString != "")
            {
                labelUnos.Text = "Log(" + unosDecimal1 + ")";
                labelRjesenje.Text = (Math.Log(unosDecimal1)).ToString();
                Ans = Math.Log(unosDecimal1);
                gotovo = true;
                unosDecimal1 = Ans;
                ansPrazan = false;
                praviAnsPostoji = true;
                operacija = false;
            }
        }

        private void ButtonSqrt_Click(object sender, EventArgs e)
        {
            if (operacija == false && unosString != "")
            {
                labelUnos.Text = "Sqrt(" + unosDecimal1 + ")";
                labelRjesenje.Text = (Math.Sqrt(unosDecimal1)).ToString();
                Ans = Math.Sqrt(unosDecimal1);
                unosDecimal1 = Ans;
                ansPrazan = false;
                praviAnsPostoji = true;
                operacija = false;
                gotovo = true;
            }
        }

        private void ButtonAbs_Click(object sender, EventArgs e)
        {
            if (operacija == false && unosString != "")
            {
                labelUnos.Text = "|" + unosDecimal1 + "|";
                labelRjesenje.Text = Math.Abs(unosDecimal1).ToString();
                Ans = Math.Abs(unosDecimal1);
                unosDecimal1 = Ans;
                ansPrazan = false;
                praviAnsPostoji = true;
                operacija = false;
                gotovo = true;
            }
        }

        private void ButtonJednako_Click(object sender, EventArgs e)
        {
            if (operand == 0)
            {
                Ans = unosDecimal1;
                labelRjesenje.Text = (unosDecimal1).ToString();
                unosDecimal1 = Ans;
                praviAnsPostoji = true;
                ansPrazan = false;
                operacija = false;
                gotovo = true;
            }

            if (gotovo == false && unosString2 != "")
            {
                switch (operand)
                {
                    case 1:
                        Ans = unosDecimal1 + unosDecimal2;
                        labelRjesenje.Text = (unosDecimal1 + unosDecimal2).ToString();
                        break;
                    case 2:
                        Ans = unosDecimal1 - unosDecimal2;
                        labelRjesenje.Text = (unosDecimal1 - unosDecimal2).ToString();
                        break;
                    case 3:
                        Ans = unosDecimal1 * unosDecimal2;
                        labelRjesenje.Text = (unosDecimal1 * unosDecimal2).ToString();
                        break;
                    case 4:
                        Ans = unosDecimal1 / unosDecimal2;
                        labelRjesenje.Text = (unosDecimal1 / unosDecimal2).ToString();
                        break;
                    default:
                        break;
                }
                unosDecimal1 = Ans;
                praviAnsPostoji = true;
                ansPrazan = false;
                operacija = false;
                gotovo = true;
                operand = 0;
            }
        }

        private void ButtonBrisi_Click(object sender, EventArgs e)
        {
            Brisi();
        }

        private void Brisi()
        {
            labelUnos.Text = "";
            labelRjesenje.Text = "";
            unosString = "";
            unosString2 = "";
            tocka = false;
            operacija = false;
            unosDecimal1 = 0;
            unosDecimal2 = 0;
            ansPrazan = true;
            gotovo = false;
        }

        private void ButtonAns_Click(object sender, EventArgs e)
        {
            if (praviAnsPostoji == true)
            {
                if (operacija == false)
                {
                    labelRjesenje.Text = "";
                    unosDecimal1 = Ans;
                    unosString = Ans.ToString();
                    labelUnos.Text = unosString;
                }
                else
                {
                    unosDecimal2 = Ans;
                    unosString2 = Ans.ToString();
                    unosString = unosString + Ans.ToString();
                    labelUnos.Text = unosString;
                }
            }
        }
    }
}
