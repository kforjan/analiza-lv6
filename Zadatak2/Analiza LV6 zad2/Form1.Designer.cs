namespace Analiza_LV6_zad2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelRijec = new System.Windows.Forms.Label();
            this.labelZivoti = new System.Windows.Forms.Label();
            this.buttonNew = new System.Windows.Forms.Button();
            this.buttonA = new System.Windows.Forms.Button();
            this.buttonB = new System.Windows.Forms.Button();
            this.buttonC = new System.Windows.Forms.Button();
            this.buttonD = new System.Windows.Forms.Button();
            this.buttonE = new System.Windows.Forms.Button();
            this.buttonF = new System.Windows.Forms.Button();
            this.buttonG = new System.Windows.Forms.Button();
            this.buttonH = new System.Windows.Forms.Button();
            this.buttonI = new System.Windows.Forms.Button();
            this.buttonJ = new System.Windows.Forms.Button();
            this.buttonU = new System.Windows.Forms.Button();
            this.buttonT = new System.Windows.Forms.Button();
            this.buttonS = new System.Windows.Forms.Button();
            this.buttonR = new System.Windows.Forms.Button();
            this.buttonP = new System.Windows.Forms.Button();
            this.buttonO = new System.Windows.Forms.Button();
            this.buttonN = new System.Windows.Forms.Button();
            this.buttonM = new System.Windows.Forms.Button();
            this.buttonL = new System.Windows.Forms.Button();
            this.buttonK = new System.Windows.Forms.Button();
            this.buttonz = new System.Windows.Forms.Button();
            this.buttonV = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelRijec
            // 
            this.labelRijec.AutoSize = true;
            this.labelRijec.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRijec.Location = new System.Drawing.Point(12, 32);
            this.labelRijec.Name = "labelRijec";
            this.labelRijec.Size = new System.Drawing.Size(81, 29);
            this.labelRijec.TabIndex = 0;
            this.labelRijec.Text = "label1";
            // 
            // labelZivoti
            // 
            this.labelZivoti.AutoSize = true;
            this.labelZivoti.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelZivoti.Location = new System.Drawing.Point(419, 68);
            this.labelZivoti.Name = "labelZivoti";
            this.labelZivoti.Size = new System.Drawing.Size(53, 20);
            this.labelZivoti.TabIndex = 1;
            this.labelZivoti.Text = "label1";
            // 
            // buttonNew
            // 
            this.buttonNew.Location = new System.Drawing.Point(397, 40);
            this.buttonNew.Name = "buttonNew";
            this.buttonNew.Size = new System.Drawing.Size(104, 23);
            this.buttonNew.TabIndex = 2;
            this.buttonNew.Text = "Nova igra";
            this.buttonNew.UseVisualStyleBackColor = true;
            this.buttonNew.Click += new System.EventHandler(this.ButtonNew_Click);
            // 
            // buttonA
            // 
            this.buttonA.Location = new System.Drawing.Point(38, 157);
            this.buttonA.Name = "buttonA";
            this.buttonA.Size = new System.Drawing.Size(21, 23);
            this.buttonA.TabIndex = 3;
            this.buttonA.Text = "A";
            this.buttonA.UseVisualStyleBackColor = true;
            this.buttonA.Click += new System.EventHandler(this.buttonA_Click);
            // 
            // buttonB
            // 
            this.buttonB.Location = new System.Drawing.Point(65, 157);
            this.buttonB.Name = "buttonB";
            this.buttonB.Size = new System.Drawing.Size(21, 23);
            this.buttonB.TabIndex = 4;
            this.buttonB.Text = "B";
            this.buttonB.UseVisualStyleBackColor = true;
            this.buttonB.Click += new System.EventHandler(this.buttonB_Click);
            // 
            // buttonC
            // 
            this.buttonC.Location = new System.Drawing.Point(92, 157);
            this.buttonC.Name = "buttonC";
            this.buttonC.Size = new System.Drawing.Size(21, 23);
            this.buttonC.TabIndex = 5;
            this.buttonC.Text = "C";
            this.buttonC.UseVisualStyleBackColor = true;
            this.buttonC.Click += new System.EventHandler(this.buttonC_Click);
            // 
            // buttonD
            // 
            this.buttonD.Location = new System.Drawing.Point(119, 157);
            this.buttonD.Name = "buttonD";
            this.buttonD.Size = new System.Drawing.Size(21, 23);
            this.buttonD.TabIndex = 6;
            this.buttonD.Text = "D";
            this.buttonD.UseVisualStyleBackColor = true;
            this.buttonD.Click += new System.EventHandler(this.buttonD_Click);
            // 
            // buttonE
            // 
            this.buttonE.Location = new System.Drawing.Point(146, 157);
            this.buttonE.Name = "buttonE";
            this.buttonE.Size = new System.Drawing.Size(21, 23);
            this.buttonE.TabIndex = 7;
            this.buttonE.Text = "E";
            this.buttonE.UseVisualStyleBackColor = true;
            this.buttonE.Click += new System.EventHandler(this.buttonE_Click);
            // 
            // buttonF
            // 
            this.buttonF.Location = new System.Drawing.Point(173, 157);
            this.buttonF.Name = "buttonF";
            this.buttonF.Size = new System.Drawing.Size(21, 23);
            this.buttonF.TabIndex = 8;
            this.buttonF.Text = "F";
            this.buttonF.UseVisualStyleBackColor = true;
            this.buttonF.Click += new System.EventHandler(this.buttonF_Click);
            // 
            // buttonG
            // 
            this.buttonG.Location = new System.Drawing.Point(200, 157);
            this.buttonG.Name = "buttonG";
            this.buttonG.Size = new System.Drawing.Size(21, 23);
            this.buttonG.TabIndex = 9;
            this.buttonG.Text = "G";
            this.buttonG.UseVisualStyleBackColor = true;
            this.buttonG.Click += new System.EventHandler(this.buttonG_Click);
            // 
            // buttonH
            // 
            this.buttonH.Location = new System.Drawing.Point(227, 157);
            this.buttonH.Name = "buttonH";
            this.buttonH.Size = new System.Drawing.Size(21, 23);
            this.buttonH.TabIndex = 10;
            this.buttonH.Text = "H";
            this.buttonH.UseVisualStyleBackColor = true;
            this.buttonH.Click += new System.EventHandler(this.buttonH_Click);
            // 
            // buttonI
            // 
            this.buttonI.Location = new System.Drawing.Point(254, 157);
            this.buttonI.Name = "buttonI";
            this.buttonI.Size = new System.Drawing.Size(21, 23);
            this.buttonI.TabIndex = 11;
            this.buttonI.Text = "I";
            this.buttonI.UseVisualStyleBackColor = true;
            this.buttonI.Click += new System.EventHandler(this.buttonI_Click);
            // 
            // buttonJ
            // 
            this.buttonJ.Location = new System.Drawing.Point(281, 157);
            this.buttonJ.Name = "buttonJ";
            this.buttonJ.Size = new System.Drawing.Size(21, 23);
            this.buttonJ.TabIndex = 12;
            this.buttonJ.Text = "J";
            this.buttonJ.UseVisualStyleBackColor = true;
            this.buttonJ.Click += new System.EventHandler(this.buttonJ_Click);
            // 
            // buttonU
            // 
            this.buttonU.Location = new System.Drawing.Point(281, 186);
            this.buttonU.Name = "buttonU";
            this.buttonU.Size = new System.Drawing.Size(21, 23);
            this.buttonU.TabIndex = 22;
            this.buttonU.Text = "U";
            this.buttonU.UseVisualStyleBackColor = true;
            this.buttonU.Click += new System.EventHandler(this.buttonU_Click);
            // 
            // buttonT
            // 
            this.buttonT.Location = new System.Drawing.Point(254, 186);
            this.buttonT.Name = "buttonT";
            this.buttonT.Size = new System.Drawing.Size(21, 23);
            this.buttonT.TabIndex = 21;
            this.buttonT.Text = "T";
            this.buttonT.UseVisualStyleBackColor = true;
            this.buttonT.Click += new System.EventHandler(this.buttonT_Click);
            // 
            // buttonS
            // 
            this.buttonS.Location = new System.Drawing.Point(227, 186);
            this.buttonS.Name = "buttonS";
            this.buttonS.Size = new System.Drawing.Size(21, 23);
            this.buttonS.TabIndex = 20;
            this.buttonS.Text = "S";
            this.buttonS.UseVisualStyleBackColor = true;
            this.buttonS.Click += new System.EventHandler(this.buttonS_Click);
            // 
            // buttonR
            // 
            this.buttonR.Location = new System.Drawing.Point(200, 186);
            this.buttonR.Name = "buttonR";
            this.buttonR.Size = new System.Drawing.Size(21, 23);
            this.buttonR.TabIndex = 19;
            this.buttonR.Text = "R";
            this.buttonR.UseVisualStyleBackColor = true;
            this.buttonR.Click += new System.EventHandler(this.buttonR_Click);
            // 
            // buttonP
            // 
            this.buttonP.Location = new System.Drawing.Point(173, 186);
            this.buttonP.Name = "buttonP";
            this.buttonP.Size = new System.Drawing.Size(21, 23);
            this.buttonP.TabIndex = 18;
            this.buttonP.Text = "P";
            this.buttonP.UseVisualStyleBackColor = true;
            this.buttonP.Click += new System.EventHandler(this.buttonP_Click);
            // 
            // buttonO
            // 
            this.buttonO.Location = new System.Drawing.Point(146, 186);
            this.buttonO.Name = "buttonO";
            this.buttonO.Size = new System.Drawing.Size(21, 23);
            this.buttonO.TabIndex = 17;
            this.buttonO.Text = "O";
            this.buttonO.UseVisualStyleBackColor = true;
            this.buttonO.Click += new System.EventHandler(this.buttonO_Click);
            // 
            // buttonN
            // 
            this.buttonN.Location = new System.Drawing.Point(119, 186);
            this.buttonN.Name = "buttonN";
            this.buttonN.Size = new System.Drawing.Size(21, 23);
            this.buttonN.TabIndex = 16;
            this.buttonN.Text = "N";
            this.buttonN.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonN.UseVisualStyleBackColor = true;
            this.buttonN.Click += new System.EventHandler(this.buttonN_Click);
            // 
            // buttonM
            // 
            this.buttonM.Location = new System.Drawing.Point(92, 186);
            this.buttonM.Name = "buttonM";
            this.buttonM.Size = new System.Drawing.Size(21, 23);
            this.buttonM.TabIndex = 15;
            this.buttonM.Text = "M";
            this.buttonM.UseVisualStyleBackColor = true;
            this.buttonM.Click += new System.EventHandler(this.buttonM_Click);
            // 
            // buttonL
            // 
            this.buttonL.Location = new System.Drawing.Point(65, 186);
            this.buttonL.Name = "buttonL";
            this.buttonL.Size = new System.Drawing.Size(21, 23);
            this.buttonL.TabIndex = 14;
            this.buttonL.Text = "L";
            this.buttonL.UseVisualStyleBackColor = true;
            this.buttonL.Click += new System.EventHandler(this.buttonL_Click);
            // 
            // buttonK
            // 
            this.buttonK.Location = new System.Drawing.Point(38, 186);
            this.buttonK.Name = "buttonK";
            this.buttonK.Size = new System.Drawing.Size(21, 23);
            this.buttonK.TabIndex = 13;
            this.buttonK.Text = "K";
            this.buttonK.UseVisualStyleBackColor = true;
            this.buttonK.Click += new System.EventHandler(this.buttonK_Click);
            // 
            // buttonz
            // 
            this.buttonz.Location = new System.Drawing.Point(65, 215);
            this.buttonz.Name = "buttonz";
            this.buttonz.Size = new System.Drawing.Size(21, 23);
            this.buttonz.TabIndex = 24;
            this.buttonz.Text = "Z";
            this.buttonz.UseVisualStyleBackColor = true;
            this.buttonz.Click += new System.EventHandler(this.buttonz_Click);
            // 
            // buttonV
            // 
            this.buttonV.Location = new System.Drawing.Point(38, 215);
            this.buttonV.Name = "buttonV";
            this.buttonV.Size = new System.Drawing.Size(21, 23);
            this.buttonV.TabIndex = 23;
            this.buttonV.Text = "V";
            this.buttonV.UseVisualStyleBackColor = true;
            this.buttonV.Click += new System.EventHandler(this.buttonV_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 360);
            this.Controls.Add(this.buttonz);
            this.Controls.Add(this.buttonV);
            this.Controls.Add(this.buttonU);
            this.Controls.Add(this.buttonT);
            this.Controls.Add(this.buttonS);
            this.Controls.Add(this.buttonR);
            this.Controls.Add(this.buttonP);
            this.Controls.Add(this.buttonO);
            this.Controls.Add(this.buttonN);
            this.Controls.Add(this.buttonM);
            this.Controls.Add(this.buttonL);
            this.Controls.Add(this.buttonK);
            this.Controls.Add(this.buttonJ);
            this.Controls.Add(this.buttonI);
            this.Controls.Add(this.buttonH);
            this.Controls.Add(this.buttonG);
            this.Controls.Add(this.buttonF);
            this.Controls.Add(this.buttonE);
            this.Controls.Add(this.buttonD);
            this.Controls.Add(this.buttonC);
            this.Controls.Add(this.buttonB);
            this.Controls.Add(this.buttonA);
            this.Controls.Add(this.buttonNew);
            this.Controls.Add(this.labelZivoti);
            this.Controls.Add(this.labelRijec);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form1";
            this.Text = "�";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelRijec;
        private System.Windows.Forms.Label labelZivoti;
        private System.Windows.Forms.Button buttonNew;
        private System.Windows.Forms.Button buttonA;
        private System.Windows.Forms.Button buttonB;
        private System.Windows.Forms.Button buttonC;
        private System.Windows.Forms.Button buttonD;
        private System.Windows.Forms.Button buttonE;
        private System.Windows.Forms.Button buttonF;
        private System.Windows.Forms.Button buttonG;
        private System.Windows.Forms.Button buttonH;
        private System.Windows.Forms.Button buttonI;
        private System.Windows.Forms.Button buttonJ;
        private System.Windows.Forms.Button buttonU;
        private System.Windows.Forms.Button buttonT;
        private System.Windows.Forms.Button buttonS;
        private System.Windows.Forms.Button buttonR;
        private System.Windows.Forms.Button buttonP;
        private System.Windows.Forms.Button buttonO;
        private System.Windows.Forms.Button buttonN;
        private System.Windows.Forms.Button buttonM;
        private System.Windows.Forms.Button buttonL;
        private System.Windows.Forms.Button buttonK;
        private System.Windows.Forms.Button buttonz;
        private System.Windows.Forms.Button buttonV;
    }
}

