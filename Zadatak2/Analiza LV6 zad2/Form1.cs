using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Analiza_LV6_zad2
{
//Napravite jednostavnu igru vjesala.Pojmovi se ucitavaju u listu iz datoteke, i u
//svakoj partiji  se odabire  nasumicni pojam  iz liste.  Omoguciti svu
//funkcionalnost koju biste ocekivali od takve igre.Nije nuzno crtati vjesala,
//dovoljno je na labeli ispisati koliko je pokusaja za odabir slova preostalo. 
    public partial class Form1 : Form
    {
        List<string> listWord = new List<string>();
        string path = "D://data.txt"; // data.txt je unutar projekta
        int trazenaRijecIndex;
        int zivoti = 5;
        int brojPogodaka;
        string trazenaRijec;
        string crtice;
        System.Text.StringBuilder crticeBuild;
        public Form1()
        {
            InitializeComponent();
            using (System.IO.StreamReader reader = new System.IO.StreamReader(@path))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    listWord.Add(line);
                }
            }
            Nova();
        }

        private void ButtonNew_Click(object sender, EventArgs e)
        {
            Nova();
        }

        public void Nova()
        {
            crtice = "";
            brojPogodaka = 0;
            zivoti = 5;
            Random rnd = new Random();
            trazenaRijecIndex = rnd.Next(0, listWord.Count);
            trazenaRijec = listWord[trazenaRijecIndex];
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                crtice += "_ ";
            }
            crticeBuild = new StringBuilder(crtice);
            labelZivoti.Text = zivoti.ToString();
            labelRijec.Text = crtice;
            EnableAll();
        }

        public void ProvjeriZivote()
        {
            if(zivoti <= 0)
            {
                
                MessageBox.Show($"Ostali ste bez zivota!\nPokusajte ponovo.\nTrazena rijec je bila: {trazenaRijec}!");
                Nova();
            }
        }

        public void ProvjeriPobjedu()
        {
            if(brojPogodaka == trazenaRijec.Length)
            {
                MessageBox.Show("Pobjedili ste!\nCestitamo!!!");
                Nova();
            }
        }

        private void buttonA_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for(int i = 0; i<trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'a')
                {
                    brojPogodaka++;
                    crticeBuild[i*2] = 'a';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonA.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonB_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'b')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 'b';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonB.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'c')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 'c';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonC.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonD_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'd')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 'd';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonD.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonE_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'e')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 'e';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonE.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonF_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'f')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 'f';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonF.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonG_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'g')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 'g';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonG.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonH_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'h')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 'h';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonH.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonI_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'i')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 'i';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonI.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonJ_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'j')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 'j';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonJ.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonK_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'k')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 'k';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonK.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonL_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'l')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 'l';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonL.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonM_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'm')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 'm';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonM.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonN_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'n')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 'n';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonN.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonO_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'o')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 'o';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonO.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonP_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'p')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 'p';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonP.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonR_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'r')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 'r';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonR.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonS_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 's')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 's';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonS.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonT_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 't')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 't';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonT.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonU_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'u')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 'u';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonU.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonV_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'v')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 'v';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonV.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        private void buttonz_Click(object sender, EventArgs e)
        {
            bool pogodak = false;
            for (int i = 0; i < trazenaRijec.Length; i++)
            {
                if (trazenaRijec[i] == 'z')
                {
                    brojPogodaka++;
                    crticeBuild[i * 2] = 'z';
                    crtice = crticeBuild.ToString();
                    pogodak = true;
                    continue;
                }
            }
            labelRijec.Text = crtice;
            if (!pogodak)
            {
                zivoti--;
                labelZivoti.Text = zivoti.ToString();
            }
            buttonz.Enabled = false;
            ProvjeriPobjedu();
            ProvjeriZivote();
        }

        public void EnableAll()
        {
            buttonA.Enabled = true;
            buttonB.Enabled = true;
            buttonC.Enabled = true;
            buttonD.Enabled = true;
            buttonE.Enabled = true;
            buttonF.Enabled = true;
            buttonG.Enabled = true;
            buttonH.Enabled = true;
            buttonI.Enabled = true;
            buttonJ.Enabled = true;
            buttonK.Enabled = true;
            buttonL.Enabled = true;
            buttonM.Enabled = true;
            buttonN.Enabled = true;
            buttonO.Enabled = true;
            buttonP.Enabled = true;
            buttonR.Enabled = true;
            buttonS.Enabled = true;
            buttonT.Enabled = true;
            buttonU.Enabled = true;
            buttonV.Enabled = true;
            buttonz.Enabled = true;
        }
    }
}
